﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fractal
{
    class Fraction
    {
        protected long Numer;
        protected long Denom;

        public Fraction(long num, long denom)
        {
            Numer = num;
            Denom = denom;
        }

        public static Fraction operator +(Fraction a, Fraction b)
        {


            return new Fraction(a.Numer * b.Denom +
                             a.Denom * b.Numer,
                             a.Denom * b.Denom);
        }

        public static Fraction operator -(Fraction a, Fraction b)
        {
           
                
            return new Fraction(a.Numer * b.Denom -
                             a.Denom * b.Numer,
                             a.Denom * b.Denom);
        }

        public static Fraction operator *(Fraction a, Fraction b)
        {
            
                
            return new Fraction(a.Numer * b.Numer,
                             a.Denom * b.Denom);
        }

        public static Fraction operator /(Fraction a, Fraction b)
        {
            
                
            return new Fraction(a.Numer * b.Denom,
                             a.Denom * b.Numer);
        }

        public static Fraction operator --(Fraction a)
        {


            return new Fraction(a.Numer - a.Denom,
                             a.Denom);
        }

        public static Fraction operator ++(Fraction a)
        {
           
                
            return new Fraction(a.Numer + a.Denom,
                             a.Denom);
        }

        public static double ToDouble(Fraction a)
        {
            
            return Convert.ToDouble(a.Numer) / Convert.ToDouble(a.Denom); 
        }

        public static string ToString(Fraction a)
        {
            return $"{a.Numer}/{a.Denom}";
        }

        public static Fraction Normalization(Fraction a)
        {
            return new Fraction(a.Numer / GetCommonDivisor(a.Numer, a.Denom), a.Denom / GetCommonDivisor(a.Numer, a.Denom));
        }

        private static long GetCommonDivisor(long i, long j)
        {
            i = Math.Abs(i);
            j = Math.Abs(j);
            while (i != j)
                if (i > j) { i -= j; }
                else { j -= i; }
            return i;
        }

        public static Fraction ReadFraction()
        {
            string info;
            Console.Write("Введіть дріб виду чисельник/знаменник:");
            info = Console.ReadLine();

            Regex reg = new Regex(@"(\d+)/(\d+)");
            Match m = reg.Match(info);
            return new Fraction(Convert.ToInt64 (m.Groups[1].Value), Convert.ToInt64(m.Groups[2].Value));


        }
    }
}
