﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fractal
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.Title = "Лабораторна робота №9";
            Console.SetWindowSize(100, 25);
            Console.BackgroundColor = ConsoleColor.White; Console.ForegroundColor = ConsoleColor.DarkBlue; Console.Clear();



            int x = 1;

            while (x != 0)
            {
                Console.WriteLine("\t1. Бінарні дії з дробами\n\t" +
                    "2. Унарні дії з дробами\n\t" +
                    "3. Порівняння дробів\n\t" +
                    "4. Скоротити дріб\n\t" +
                    "0. Вихід" +
                    "\n-->");

                while (!int.TryParse(Console.ReadLine(), out x))
                {
                    Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");
                }


                switch (x)
                {
                    case 1: { Binary(); break; }
                    case 2: { Unary(); break; }
                    case 3: { CompareFrac(); break; }
                    case 4: { Norm(); break; }
                }

            }
        }

        private static void Norm()
        {
          Console.WriteLine( Fraction.ToString( Fraction.Normalization(Fraction.ReadFraction())));
        }

        private static void CompareFrac()
        {
            
            double a = Fraction.ToDouble(Fraction.ReadFraction());     
            double b = Fraction.ToDouble(Fraction.ReadFraction());
            if (a == b) Console.WriteLine(" == "); 
            if (a >= b) Console.WriteLine(" >= ");
            if (a < b) Console.WriteLine(" < ");
            if (a > b) Console.WriteLine(" > ");
            if (a <= b) Console.WriteLine(" <= ");
            if (a != b) Console.WriteLine(" != ");
            
        }

        private static void Unary()
        {
            int n;
            Fraction a;
            Console.WriteLine("\t1. Інкремент\n\t" +
                   "2. Декремент\n\t" +
    
                   "\n-->");

            while (!int.TryParse(Console.ReadLine(), out n))
            {
                Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");
            }
            switch (n)
            {
                case 1 :
                    {
                        a = Fraction.ReadFraction();
                        Console.Write( Fraction.ToString(Fraction.Normalization(++a)) + "\n");
                        break; }
                case 2:
                    {
                        a = Fraction.ReadFraction();
                        Console.Write(Fraction.ToString(Fraction.Normalization(--a)) + "\n");
                        break;
                    }
            }
        }

        private static void Binary()
        {
            int n;
            Fraction a, b;
            Console.WriteLine("\t1. Додати дроби\n\t" +
                   "2. Відняти\n\t" +
                   "3. Поділити\n\t" +
                   "4. Помножити\n\t" +
                   "\n-->");

            while (!int.TryParse(Console.ReadLine(), out n))
            {
                Console.WriteLine("Помилка введення значення. Будь-ласка повторіть введення значення ще раз!");
            }
            switch (n)
            {
                case 1:
                    {
                      a = Fraction.ReadFraction();
                        b = Fraction.ReadFraction();
                        Console.Write("Сума дробів:" + Fraction.ToString(Fraction.Normalization(a+b)) + "\n");

                         break;
                    }
                case 2:
                    {
                        a = Fraction.ReadFraction();
                        b = Fraction.ReadFraction();
                        Console.Write("Різниця дробів:" + Fraction.ToString(Fraction.Normalization(a - b)) + "\n");

                        break;
                    }
                case 3:
                    {
                        a = Fraction.ReadFraction();
                        b = Fraction.ReadFraction();
                        Console.Write("Ділення дробів:" + Fraction.ToString(Fraction.Normalization(a / b)) + "\n");

                        break;
                    }
                case 4:
                    {
                        a = Fraction.ReadFraction();
                        b = Fraction.ReadFraction();
                        Console.Write("Добуток дробів:" + Fraction.ToString(Fraction.Normalization(a * b)) + "\n");

                        break;
                    }


            }
        }
    }
}
